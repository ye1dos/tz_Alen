## Running
From terminal run:
- `docker-compose up --build`


### front-end
Above commands start the front-end on: http://localhost:3000
  - React
  - Typescript

### - back-end
Above commands start the back-end on: http://localhost:5000
  - Node
  - Express
  - Typescript

### Database - from docker
  - Mongo
