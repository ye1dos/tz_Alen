import { Request, Response } from 'express';
import { User } from '../models/userModel';

const userController = {
    getAllUsers: async (req: Request | any, res: Response) => {
        const keyword = req.query.search
            ? {
                $or: [
                    { name: { $regex: req.query.search, $options: "i" } },
                    { email: { $regex: req.query.search, $options: "i" } },
                ],
            }
            : {};
        try {
            const users = req.user && (await User.find({ ...keyword, _id: { $ne: req.user._id } }).exec());
            console.log(users);
            res.send(users);
        } catch (error) {
            console.error('Error:', error);
            res.status(500).send('Internal Server Error');
        }
    },
};

export default userController;
