import { Request, Response } from 'express';
import { User } from '../models/userModel';
import { generateToken } from '../utils/generateToken';

const authController = {
  register: async (req: Request, res: Response) => {
    console.log('res')
    try {
      const { name, email, password, pic } = req.body;
      if (!name || !email || !password) {
        return res.status(400).json({
          message: 'User data is not full!'
        });
      }
      // Check if a user with the same login already exists
      const existingUser = await User.findOne({ email });
      if (existingUser) {
        return res.status(400).json({ message: 'User with this login already exists' });
      }

      // Create a new user
      const newUser: any = await User.create({
        name,
        email,
        password,
        pic,
      });

      res.status(201).json({
        ...newUser,
        token: generateToken(newUser._id),
        message: 'User registered successfully'
      });
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: 'Internal server error' });
    }
  },

  login: async (req: Request, res: Response) => {
    const { email, password } = req.body;
    const user: any = await User.findOne({ email });

    if (user && (await user.matchPassword(password))) {
      res.json({
        _id: user._id,
        name: user.name,
        email: user.email,
        isAdmin: user.isAdmin,
        pic: user.pic,
        token: generateToken(user._id),
      });
    } else {
      res.status(401);
      throw new Error("Invalid Email or Password");
    }
  },
};

export default authController;
