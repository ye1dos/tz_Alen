import { Request, Response } from 'express';
import { Document } from 'mongoose'; // Import the Document type if you need it
import { User, Chat, Message } from '../models';

const allMessages = async (req: Request, res: Response) => {
    try {
        const messages = await Message.find({ chat: req.params.chatId })
            .populate("sender", "name pic email")
            .populate("chat");

        res.json(messages);
    } catch (error: any) {
        res.status(400).json({ message: error.message });
    }
};

const sendMessage = async (req: Request | any, res: Response) => {
    const { content, chatId } = req.body;

    if (!content || !chatId) {
        console.log("Invalid data passed into request");
        return res.sendStatus(400);
    }

    const newMessage = {
        sender: req.user._id,
        content: content,
        chat: chatId,
    };

    try {
        let message: any = await Message.create(newMessage);
        message = await message.populate("sender", "name pic")
        message = await message.populate("chat")
        message = await User.populate(message, {
            path: "chat.users",
            select: "name pic email",
        });

        await Chat.findByIdAndUpdate(req.body.chatId, { latestMessage: message });

        res.json(message);
    } catch (error: any) {
        res.status(400).json({ message: error.message });
    }
};

export { allMessages, sendMessage };
