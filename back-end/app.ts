import { notFound } from './middlewares/errorMiddleware';
// Routes
import {
  auth,
  chatRoutes,
  messageRoutes,
  userRoutes,
} from './routes';

const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();

// Middleware
const corsOptions = {
  origin: '*',
};
app.use(express.json());
app.use(cors(corsOptions));
app.use(bodyParser.json());
// app.use(notFound);

app.use('/api/auth', auth);
app.use('/api/user', userRoutes);
app.use('/api/message', messageRoutes);
app.use('/api/chat', chatRoutes);

export default app;
