import jwt from 'jsonwebtoken';

export const generateToken = (id: number | string) => {
    return jwt.sign({ id }, process.env.JWT_SECRET || 'secret', {
        expiresIn: "30d",
    })
}