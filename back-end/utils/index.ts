import connectDB from './mongoose';
import { generateToken } from './generateToken';

export {
    connectDB,
    generateToken,
}
