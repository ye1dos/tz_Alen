import mongoose, { ConnectOptions } from 'mongoose';
const connectDB = async () => {
  try {
    const mongodbUrl = 'mongodb://127.0.0.1:27017/db';

    await mongoose.connect(mongodbUrl, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    } as ConnectOptions);
    console.log("connected");
  } catch (e) {
    console.log("error with connection");
  }
}

export default connectDB;
