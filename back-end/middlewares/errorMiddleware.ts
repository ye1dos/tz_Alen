import { Request, Response, NextFunction } from "express";

export const notFound = (req: Request, res: Response | any, next: NextFunction) => {
    const error = new Error(`Not Found! - ${res.originalUrl}`);
    res.status(404);
    next(error);
}
