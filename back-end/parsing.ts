import { Response } from "express";
import cors from 'cors';
const express = require('express');
const { Request, Response } = require('express');
const { Web3 } = require('web3');
const https = require('https');
import bodyParser from 'body-parser';
const app = express();
import mongoose, { ConnectOptions } from 'mongoose';

app.use(cors());
app.use(bodyParser.json());

const mongodbUrl = 'mongodb://mongo:27017/db';

mongoose.connect(mongodbUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
} as ConnectOptions);

const recordSchema = new mongoose.Schema({
  transfers: Array,
  contractAddress: String,
  token: String,
});

const Record = mongoose.model('Records', recordSchema);

// Define the parseUSDTTransfers function
async function parseUSDTTransfers(res: Response, contractAddress: string) {
  const web3 = new Web3('https://api.etherscan.io/api');
  const params = {
    module: 'logs',
    action: 'getLogs',
    fromBlock: '17612968',
    toBlock: '17612970',
    address: contractAddress,
    topic0: '0xddf252ad1be2c89b69c2b068fc378daa952ba7f163c4a11628f55a4df523b3ef',
    apikey: '1SVP8D8F6GGHJU6U9T2FRX21KNMS2FQZZQ',
  };

  const queryString = Object.entries(params)
    .map(([key, value]) => `${key}=${encodeURIComponent(value)}`)
    .join('&');

  const url = `/api?${queryString}`;

  const options = {
    hostname: 'api.etherscan.io',
    port: 443,
    path: url,
    method: 'GET',
  };

  const req = https.request(options, (response: Response) => {
    let data = '';

    response.on('data', (chunk: any) => {
      data += chunk;
    });

    response.on('end', async () => {
      const parsedData = JSON.parse(data);

      if (parsedData.status === '1') {
        const transferEvents = parsedData.result;

        const transfers = transferEvents.map((event: any) => ({
          from: event.topics[1],
          to: event.topics[2],
          value: web3.utils.fromWei(event.data, 'ether') + ' USDT',
        }));

        const record = new Record({
          transfers,
          contractAddress,
          token: 'USDT',
        });

        await record.save();
        
        res.json({ transfers });
      } else {
        res.status(500).json({ error: 'Etherscan API returned an error', message: parsedData.message });
      }
    });
  });

  req.on('error', (error: Error) => {
    res.status(500).json({ error: 'Error', message: error.message });
  });

  req.end();
}

app.get('/add-to-db', (req: typeof Request, res: Response) => {
  const { contractAddress } = req.query;
  console.log('contractAddress', contractAddress);
  parseUSDTTransfers(res, contractAddress);
});

app.post('/pull-by-contract-address', async (req: any, res: Response) => {
  try {
    const { contractAddress } = req.query;

    const records = await Record.find({ contractAddress });

    if (records.length === 0) {
      return res.status(404).json({ message: 'No records found for the provided contractAddress.' });
    }
    const transfers = records.map((record) => record.transfers);

    res.json({ transfers });
  } catch (error: any) {
    res.status(500).json({ error: 'Error', message: error.message });
  }
});

const PORT = process.env.PORT || 5000;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
