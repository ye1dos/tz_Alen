import auth from './auth';
import chatRoutes from './chatRoutes';
import userRoutes from './userRoutes';
import messageRoutes from './messageRoutes';

export {
    auth,
    chatRoutes,
    userRoutes,
    messageRoutes,
}