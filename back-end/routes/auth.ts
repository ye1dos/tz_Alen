import express from 'express';
const router = express.Router();
import authController from '../controllers/authController';

// User registration route
router.post('/register', authController.register);

// User login route
router.post('/login', authController.login);

export default router;
