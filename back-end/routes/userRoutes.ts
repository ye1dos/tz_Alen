import express from 'express';
import userController from '../controllers/userController';
import { protect } from '../middlewares/authMiddleware';

const router = express.Router();
router.get('/', protect, userController.getAllUsers);
export default router;