const axios = require("axios");

const http = axios.create({
  baseURL: "http://localhost:5000", // Your base URL
  timeout: 5000,
});

export default http;
